package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

/**
 * @author shero
 *
 */
public class JmsMessageBrokerSupport {
    
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    /**
     * @return 返回MessageBrokerSupport
     * @throws Exception
     */
    public static MessageBrokerSupport createBrokerSupportOnAvailablePort() throws Exception {
    	MessageBrokerSupport brokerSupport = new MessageBrokerSupport();
    	/*
    	 * 此处MessageBrokerHandler可以使用Spring setProperty等途径进行注入
    	 */
        brokerSupport.setBrokerHandler(new JmsMessageBrokerHandler());
        brokerSupport.setBrokerUrl(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
        brokerSupport.init();
        return brokerSupport;
    }
    
    public static MessageBrokerSupport createBrokerSupport(String brokerUrl) throws Exception {
    	MessageBrokerSupport brokerSupport = new MessageBrokerSupport();
    	brokerSupport.setBrokerHandler(new JmsMessageBrokerHandler());
    	brokerSupport.setBrokerUrl(brokerUrl);
    	return brokerSupport;
    }

}
