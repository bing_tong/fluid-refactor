package com.paic.arch.jmsbroker;

import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;

/**
 * @author shero
 * 该类封装了作为消息传递所需要的业务逻辑操作，此类只关注业务上需要的功能，不关心技术的具体实现。
 * 其中MessageBrokerHandler负责技术的具体实现，可对该接口制造切面，进行log以及异常的处理
 */
public class MessageBrokerSupport {
	private static final Logger LOG = getLogger(MessageBrokerSupport.class);
	/**
	 * 该成员变量有Set方法，可通过Spring的Setproperty动态注入，这里主要为了说明具体实现和业务逻辑的剥离，因此省去。
	 */
	private MessageBrokerHandler handler;
	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

	public MessageBrokerHandler getBrokerHandler() {
		return handler;
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) {
		MessageBean bean = new MessageBean();
		bean.setDestName(aDestinationName);
		bean.setTimeout(aTimeout);
		try {
			return handler.receiveMessage(bean);
		} catch (NoMessageReceivedException e) {
			throw e;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return "";
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
		return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
	}

	public void sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
		MessageBean bean = new MessageBean();
		bean.setDestName(aDestinationName);
		bean.setMessage(aMessageToSend);
		try {
			handler.sendMessage(bean);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public void setBrokerHandler(MessageBrokerHandler brokerHandler) {
		this.handler = brokerHandler;
	}

	public void init() throws Exception {
		this.getBrokerHandler().initBrokerHandler();
	}

	public void setBrokerUrl(String brokerUrl) {
		this.getBrokerHandler().setBrokerUrl(brokerUrl);
	}

	public String getBrokerUrl() {
		return this.getBrokerHandler().getBrokerUrl();
	}

	public void finalizeBrokerHandler() {
		try {
			this.getBrokerHandler().finalizeBrokerHandler();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public long getEnqueuedMessage(String destName) {
		try {
			return this.getBrokerHandler().getEnqueuedMessage(destName);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return 0;
	}
}
