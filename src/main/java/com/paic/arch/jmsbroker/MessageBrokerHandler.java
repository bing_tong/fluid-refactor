package com.paic.arch.jmsbroker;

/**
 * @author shero
 * 该接口描述了消息队列操作需要的具体接口，具体的技术类可实现该接口满足业务需求，另外可通过这个接口进行动态代理制造切面。
 */
public interface MessageBrokerHandler {

	void setBrokerUrl(String brokerUrl);
	
	String getBrokerUrl();
	
	void sendMessage(MessageBean bean) throws Exception;
	
	String receiveMessage(MessageBean bean) throws Exception;

	void initBrokerHandler() throws Exception;
	
	void finalizeBrokerHandler() throws Exception;
	
	long getEnqueuedMessage(String destName) throws Exception;
}
