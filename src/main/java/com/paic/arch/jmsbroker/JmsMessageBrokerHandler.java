package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;

/**
 * @author shero
 * 通过JMS消息队列技术，实现了队列的业务操作需求
 */
public class JmsMessageBrokerHandler implements MessageBrokerHandler {
	private Connection connection;
	private String brokerUrl;
	private BrokerService brokerService;

	@Override
	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	private void initConnection() throws Exception {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(this.brokerUrl);
		connection = connectionFactory.createConnection();
		connection.start();
	}

	@Override
	public void initBrokerHandler() throws Exception {
		brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
        brokerService.start();
	}

	@Override
	public void finalizeBrokerHandler() throws Exception{
		if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
	}

	@Override
	public String getBrokerUrl() {
		return this.brokerUrl;
	}

	@Override
	public long getEnqueuedMessage(String destName) throws Exception {
		Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(destName)) {
                return destination.getDestinationStatistics().getMessages().getCount();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", destName, brokerUrl));
	}

	@Override
	public void sendMessage(MessageBean bean) throws Exception {
		initConnection();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue(bean.getDestName());
		MessageProducer producer = session.createProducer(queue);
		producer.send(session.createTextMessage(bean.getMessage()));
		producer.close();
	}

	@Override
	public String receiveMessage(MessageBean bean) throws Exception {
		initConnection();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue(bean.getDestName());
		MessageConsumer consumer = session.createConsumer(queue);
		Message message = consumer.receive(bean.getTimeout());
		if (message == null) {
			throw new NoMessageReceivedException(
					String.format("No messages received from the broker within the %d timeout", bean.getTimeout()));
		}
		consumer.close();
		return ((TextMessage) message).getText();
	}

}
